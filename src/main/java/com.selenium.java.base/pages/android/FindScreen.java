package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class FindScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Kobiety\"]")
    public WebElement btn_women;

    @FindBy (how = How.XPATH, using = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Mężczyźni\"]")
    public WebElement btn_men;

    @FindBy (how = How.XPATH, using = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Dzieci\"]")
    public WebElement btn_children;

    public void checkFindUI() {

        btn_men.click();
        pause(1500);
        btn_children.click();
        pause(1500);
        btn_women.click();
        pause(1500);

    }

}

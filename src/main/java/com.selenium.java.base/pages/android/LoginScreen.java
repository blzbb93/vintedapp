package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;
import java.util.NoSuchElementException;

public class LoginScreen extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @FindBy(how = How.ID, using = "pl.vinted:id/email_login_action_button")
    public WebElement btn_login1;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout[2]/android.widget.Button[2]")
    public WebElement btn_login2;

    @FindBy(how = How.ID, using = "pl.vinted:id/show_login_options_button")
    public WebElement btn_login3;

    @FindBy(how = How.ID, using = "pl.vinted:id/login_username")
    public WebElement input_email;

//    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.EditText")
//    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.vinted:id/login_password_input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.vinted:id/login_login")
    public WebElement btn_getLogin;

    //pl.vinted:id/login_login

    @FindBy(how = How.ID, using = "com.google.android.gms:id/cancel")
    public WebElement btn_cancel;

    @Step("")
    public void loginToApp(String name, String pass) {

        //region ALTERNATE_LIST_ELEMENT_CHECK
//        WebDriver driver = getDriver();
//        List<WebElement> elements = driver.findElements(By.id("email_login_action_button"));
//        if (elements.size() > 0)
//            elements.get(0).click();
//        System.out.println("register 1");
//
//        List<WebElement> elementsb = driver.findElements(By.id("show_login_options_button"));
//        if (elementsb.size() > 0)
//            elementsb.get(0).click();
//        System.out.println("register 2");
//
//        List<WebElement> elementsc = driver.findElements(By.id("welcome_btn_sign_in"));
//        if (elementsc.size() > 0)
//            elementsc.get(0).click();
//        System.out.println("register 3");
        //endregion ALTERNATE_LIST_ELEMENT_CHECK

        //region ALTERNATE_TRYCATCH
//        try {
//            btn_login1.click();
//            System.out.println("Logowanie btn_login1.");
//        } catch (NoSuchElementException e) {
//            try {
//                btn_login2.click();
//                System.out.println("Logowanie btn_login2.");
//            } catch (NoSuchElementException f) {
//                btn_login3.click();
//                System.out.println("Logowanie btn_login3.");
//            }
//        }
        //endregion ALTERNATE_TRYCATCH

        //region ALTERNATE_IF_EXIST
        helper.clickIfExistById("pl.vinted:id/email_login_action_button");
        helper.clickIfExistById("pl.vinted:id/show_login_options_button");
        helper.clickIfExistById("pl.vinted:id/email_action_button");
        helper.clickIfExistById("pl.vinted:id/welcome_btn_sign_in");
        //endregion ALTERNATE_IF_EXIST

        //      btn_cancel.click();

        helper.waitTime(1);
        helper.clickIfExistById("email_action_button");

        helper.waitTime(1);
        helper.tapCoordinates(70, 180);
        helper.waitTime(1);
        input_email.sendKeys(name);
        helper.waitTime(1);
        input_password.sendKeys(pass);
        btn_getLogin.click();
    }
}
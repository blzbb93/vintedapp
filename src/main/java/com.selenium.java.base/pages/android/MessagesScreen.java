package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MessagesScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "(//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"tab\"])[1]")
    public WebElement btn_messages;

    @FindBy(how = How.XPATH, using = "(//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"tab\"])[2]")
    public WebElement btn_notifications;


    public void checkMessagesUI() {

        btn_notifications.click();
        pause(1500);
        btn_messages.click();
        pause(1500);

    }

}

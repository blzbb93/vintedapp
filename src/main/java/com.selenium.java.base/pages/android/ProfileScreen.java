package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProfileScreen extends BaseTest {



    @FindBy(how = How.ID, using = "pl.vinted:id/user_short_info_cell")
    public WebElement btn_showProfile;

    //region SHOWPROFILEUIFINDERS

    @FindBy(how = How.XPATH, using = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Szafa\"]")
    public WebElement btn_shelf;

    @FindBy(how = How.XPATH, using = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Komentarze\"]")
    public WebElement btn_comments;

    @FindBy(how = How.XPATH, using = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"O mnie\"]")
    public WebElement btn_aboutMe;

    @FindBy(how = How.ID, using = "pl.vinted:id/actionbar_button")
    public WebElement btn_goBack;

    //endregion SHOWPROFILEUIFINDERS


    public void checkProfileUI() {

        btn_showProfile.click();
        pause(1000);

        btn_comments.click();
        pause(1000);
        btn_aboutMe.click();
        pause(1000);
        btn_shelf.click();
        pause(1000);

        btn_goBack.click();
        pause(1000);

    }


}

package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import javafx.scene.chart.ScatterChart;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

public class RegisterScreen extends BaseTest {

    MethodHelper helper = new MethodHelper();
    WebDriver driver = getDriver();

    //region FindBy
    @FindBy(how = How.ID, using = "pl.vinted:id/welcome_btn_sign_up")
    public WebElement btn_register1;

    @FindBy(how = How.ID, using = "email_register_action_button")
    public WebElement btn_register2;

    @FindBy(how = How.ID, using = "pl.vinted:id/show_registration_options_button")
    public WebElement btn_register3;

    @FindBy(how = How.ID, using = "com.google.android.gms:id/cancel")
    public WebElement btn_cancel;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.EditText\n")
    public WebElement input_nameSurname;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.EditText\n")
    public WebElement input_profileName;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.EditText\n")
    public WebElement input_email;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.EditText\n")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "newsletter_subscription")
    public WebElement btn_newsletter;

    @FindBy(how = How.ID, using = "email_register_sign_up")
    public WebElement btn_registerButton;

    @FindBy(how = How.ID, using = "com.google.android.gms:id/credential_save_reject")
    public WebElement btn_never;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.Button[2]")
    public WebElement btn_acceptAll;

    @FindBy(how = How.ID, using = "pl.vinted:id/onboarding_navigate_upload_button")
    public WebElement btn_add;
    //endregion FindBy

    @Step("1c")
    public void registerToApp(String name, String profileName, String email, String password) {

        try {
            btn_register1.click();
        } catch (NoSuchElementException e) {
            try {
                btn_register2.click();
            } catch (NoSuchElementException f) {
                btn_register3.click();
            }
        }

//        helper.clickIfExistById("pl.vinted:id/welcome_btn_sign_up"); //Ekran 3
//        helper.clickIfExistById("email_register_action_button"); //Ekran 2
//        helper.clickIfExistById("pl.vinted:id/show_registration_options_button"); //Ekran 1

        helper.clickIfExistById("email_action_button");

        input_nameSurname.sendKeys(name);
        input_profileName.sendKeys(profileName);
        input_email.sendKeys(email);
        input_password.sendKeys(password);
        btn_newsletter.click();
        btn_registerButton.click();
        helper.waitTime(4);

        helper.clickIfExistByXpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.Button[2]");
//        btn_acceptAll.click();

    }
}













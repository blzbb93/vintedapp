package com.selenium.java.tests.test.android;
import com.google.common.collect.ImmutableMap;
import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.helper.TestListener;
import com.selenium.java.base.pages.android.LoginScreen;
import io.appium.java_client.functions.ExpectedCondition;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import java.lang.reflect.Method;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
@Listeners({ TestListener.class })
public class LoginTest extends BaseTest {

    WebDriver driver = getDriver();

    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {
        instalApp = false;
        launchAndroid(platform, deviceID, deviceName, context);
    }
    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                  {"lukasz_tester_1@wp.pl", "Tester123##"},
                
//                {"lukasz_tester_2@wp.pl", "Tester123##"},
//                {"lukasz_tester_333@wp.pl", "Tester123##"},
//                {"lukasz_tester_444@wp.pl", "Tester123##"},
//                {"lukasz_tester_555@wp.pl", "Tester123##"},
        };
    }
    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", ""},
//                {"lukasz_tester_2@wp.pl", ""},
//                {"lukasz_tester_333@wp.pl", ""},
//                {"lukasz_tester_444@wp.pl", ""},
//                {"lukasz_tester_555@wp.pl", ""},
        };
    }
    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"", "Tester123##"},
//                {"", "Tester123##"},
//                {"", "Tester123##"},
//                {"", "Tester123##"},
//                {"", "Tester123##"},
        };
    }
    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"lukasz_tester_1@@wpp.pl", "Tester123##"},
//                {"lukasz_tester_1@wpp", "Tester123##"},
//                {"lukasz_tester_1@", "Tester123##"},
//                {"lukasz_tester_1", "Tester123##"},
//                {"@wp.pl", "Tester123##"},
        };
    }
    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", "tester123##"},
//                {"lukasz_tester_2@wp.pl", "Tester123#"},
//                {"lukasz_tester_333@wp.pl", "tester123"},
//                {"lukasz_tester_444@wp.pl", "Tester"},
//                {"lukasz_tester_555@wp.pl", "lukasz_tester_5@wp.pl"},
        };
    }
    MethodHelper helper = new MethodHelper();

    @Test(dataProvider = "getData", description = "Checking basic functionality")

    public void testLoginToApp(String name, String password) {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        helper.waitTime(3);
        //      Assert.assertTrue(driver.findElement(By.id("TU WSTAW ID")).isDisplayed());

        //region SCREENSHOTS
        helper.getScreenshotForAllure("x");
        helper.waitTime(3);
        helper.getScreenShot("X");
        //endregion SCREENSHOTS
        try {
            driver.findElement(By.id("pl.vinted:id/user_verification_phone"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error: element not found!!");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/user_verification_phone")).isDisplayed());
        System.out.println("Element found. I'm continuing.");
    }

    @Test(dataProvider = "getLoginOnly", description = "Checking basic functionality")

    public void testLoginToAppWithData(String name, String password) {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        helper.waitTime(3);
        //      Assert.assertTrue(driver.findElement(By.id("TU WSTAW ID")).isDisplayed());

        //region SCREENSHOTS
        helper.getScreenshotForAllure("x");
        helper.waitTime(3);
        helper.getScreenShot("X");
        //endregion SCREENSHOTS
        try {
            driver.findElement(By.id("pl.vinted:id/user_verification_phone"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error: element not found!!");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/user_verification_phone")).isDisplayed());
        System.out.println("Element found. I'm continuing.");
    }

    @Test(dataProvider = "getPasswordOnly", description = "Checking basic functionality")

    public void testLoginToAppPasswordOnly(String name, String password) {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        helper.waitTime(3);
        //      Assert.assertTrue(driver.findElement(By.id("TU WSTAW ID")).isDisplayed());

        //region SCREENSHOTS
        helper.getScreenshotForAllure("x");
        helper.waitTime(3);
        helper.getScreenShot("X");
        //endregion SCREENSHOTS
        try {
            driver.findElement(By.id("pl.vinted:id/user_verification_phone"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error: element not found!!");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/user_verification_phone")).isDisplayed());
        System.out.println("Element found. I'm continuing.");
    }

    @Test(dataProvider = "getWrongMail", description = "Checking basic functionality")

    public void testLoginToAppWrongMail(String name, String password) {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        helper.waitTime(3);
        //      Assert.assertTrue(driver.findElement(By.id("TU WSTAW ID")).isDisplayed());

        //region SCREENSHOTS
        helper.getScreenshotForAllure("x");
        helper.waitTime(3);
        helper.getScreenShot("X");
        //endregion SCREENSHOTS
        try {
            driver.findElement(By.id("pl.vinted:id/user_verification_phone"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error: element not found!!");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/user_verification_phone")).isDisplayed());
        System.out.println("Element found. I'm continuing.");
    }

    @Test(dataProvider = "getWrongPassword", description = "Checking basic functionality")

    public void testLoginToAppWrongPassword(String name, String password) {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        helper.waitTime(3);
        //      Assert.assertTrue(driver.findElement(By.id("TU WSTAW ID")).isDisplayed());

        //region SCREENSHOTS
        helper.getScreenshotForAllure("x");
        helper.waitTime(3);
        helper.getScreenShot("X");
        //endregion SCREENSHOTS
        try {
            driver.findElement(By.id("pl.vinted:id/user_verification_phone"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error: element not found!!");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/user_verification_phone")).isDisplayed());
        System.out.println("Element found. I'm continuing.");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        helper.waitTime(3);
        System.out.println("Koniec testów logowania");
    }
}
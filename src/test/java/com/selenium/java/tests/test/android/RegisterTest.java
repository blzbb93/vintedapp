package com.selenium.java.tests.test.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.helper.TestListener;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.android.RegisterScreen;
import com.selenium.java.base.pages.web.LoginPage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;

@Listeners({TestListener.class})

public class RegisterTest extends BaseTest {

    MethodHelper helper = new MethodHelper();


    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceID, deviceName, context);

    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"Lukasz Blaszkiewicz", "Luktes15", "luktes15@wp.pl", "Tester123##"},
//                {"lukasz_tester_2@wp.pl", "Tester123##"},
//                {"lukasz_tester_333@wp.pl", "Tester123##"},
//                {"lukasz_tester_444@wp.pl", "Tester123##"},
//                {"lukasz_tester_555@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"Lukasz Blaszkiewicz", "TesterLukasz10", "testerlukasz10@wp.pl", ""},
//                {"Lukasz Blaszkiewicz", "LukaszTest123321I", "lukasz_tester_vntd_A1@wp.pl", ""},
        };
    }

    //
    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"Lukasz Blaszkiewicz", "LukaszTest123321I", "", "Tester123##"},
//                {"Lukasz Blaszkiewicz", "LukaszTest123321I", "", "Tester123##"},

        };
    }

    //
    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_1@w@p.pl", "Tester123##"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_1@wpp", "Tester123##"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_1@", "Tester123##"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_1", "Tester123##"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "@wp.pl", "Tester123##"},
        };
        }

        //
        @DataProvider
        public Object[][] getWrongPassword() {
            return new Object[][]{
                    {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_1q1x@wp.pl", "tester123##"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_2q2x@wp.pl", "tester"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_3q3x@wp.pl", "tester123"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_4q4x@wp.pl", "Tester"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_5q5x@wp.pl", "Tester##%"},
//                {"Lukasz Blaszkiewicz", "Testedname065", "lukasz_tester_5q5x@wp.pl", "Tester123123"},
            };
        }


        @Test(dataProvider = "getData", priority = 1, description = "1")
        @Severity(SeverityLevel.CRITICAL)
        @Story("registrationWithData")
        public void registrationWithData (String name, String profileName, String email, String password){
            WebDriver driver = getDriver();
            RegisterScreen registerScreen = PageFactory.initElements(driver, RegisterScreen.class);

            registerScreen.registerToApp(name, profileName, email, password);

            helper.waitTime(4);
//            helper.assertTrueById("pl.vinted:id/onboarding_title");
            Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/onboarding_title")).isDisplayed());

            helper.getScreenShot("registrationWithData.png");
            helper.getScreenshotForAllure("registrationWithData.png");
        }

        @Test(dataProvider = "getLoginOnly", priority = 2, description = "2")
        @Severity(SeverityLevel.NORMAL)
        @Story("registrationWithLoginOnly")
        public void registrationWithLoginOnly (String name, String profileName, String email, String password){
            WebDriver driver = getDriver();
            RegisterScreen registerScreen = PageFactory.initElements(driver, RegisterScreen.class);

            registerScreen.registerToApp(name, profileName, email, password);

            helper.waitTime(2);
            helper.assertTrueById("pl.vinted:id/modal_primary_button");
            helper.getScreenShot("registrationWithLoginOnly.png");
            helper.getScreenshotForAllure("registrationWithLoginOnly.png");
        }

        @Test(dataProvider = "getPasswordOnly", priority = 3, description = "3")
        @Severity(SeverityLevel.CRITICAL)
        @Story("passwordOnly")
        public void passwordOnly (String name, String profileName, String email, String password){
            WebDriver driver = getDriver();
            RegisterScreen registerScreen = PageFactory.initElements(driver, RegisterScreen.class);

            registerScreen.registerToApp(name, profileName, email, password);

            helper.waitTime(2);
            helper.assertTrueById("pl.vinted:id/modal_primary_button");
            helper.getScreenShot("passwordOnly.png");
            helper.getScreenshotForAllure("passwordOnly.png");
        }

        @Test(dataProvider = "getWrongMail", priority = 4, description = "4")
        @Severity(SeverityLevel.CRITICAL)
        @Story("wrongMail")
        public void wrongMail (String name, String profileName, String email, String password){
            WebDriver driver = getDriver();
            RegisterScreen registerScreen = PageFactory.initElements(driver, RegisterScreen.class);

            registerScreen.registerToApp(name, profileName, email, password);

            helper.waitTime(2);
            helper.assertTrueById("pl.vinted:id/modal_primary_button");
            helper.getScreenShot("wrongMail.png");
            helper.getScreenshotForAllure("wrongMail.png");
        }

        @Test(dataProvider = "getWrongPassword", priority = 5, description = "5")
        @Severity(SeverityLevel.CRITICAL)
        @Story("wrongPassword")
        public void wrongPassword (String name, String profileName, String email, String password){
            WebDriver driver = getDriver();
            RegisterScreen registerScreen = PageFactory.initElements(driver, RegisterScreen.class);

            registerScreen.registerToApp(name, profileName, email, password);

            helper.waitTime(2);
            helper.assertTrueById("pl.vinted:id/modal_primary_button");
            helper.getScreenShot("wrongPassword.png");
            helper.getScreenshotForAllure("wrongPassword.png");
        }

        @AfterMethod(description = "Finishing test and shutting down app")
        public void tearDown () {

        }
    }






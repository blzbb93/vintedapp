package com.selenium.java.tests.test.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.annotations.Test;

public class UIMovementTest extends BaseTest {

    WebDriver driver = getDriver();

    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {
        instalApp = false;
        launchAndroid(platform, deviceID, deviceName, context);
    }

    @DataProvider
    public Object[][] simpleData() {
        return new Object[][]{
                {"theofficialqualia@gmail.com", "Nitas1991"},
        };
    }

    MethodHelper helper = new MethodHelper();

    //region MAINUIFINDERS
    @FindBy (how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[1]")
    public WebElement btn_news;

    @FindBy (how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[2]")
    public WebElement btn_find;

    @FindBy (how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[3]")
    public WebElement btn_sell;

    @FindBy (how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[4]")
    public WebElement btn_messages;

    @FindBy (how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[5]")
    public WebElement btn_profile;
    //endregion MAINUIFINDERS

    @Test(dataProvider = "simpleData", description = "Checking UI Movement")
    public void testUIBarMovement(String name, String password) {

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);

        loginScreen.loginToApp(name, password);

        btn_find.click();
        helper.waitTime(1);
        System.out.println("I'm in finding bar.");

        btn_news.click();
        helper.waitTime(1);
        System.out.println("I'm in news bar.");

        btn_messages.click();
        helper.waitTime(1);
        System.out.println("I'm in messages bar.");

        btn_profile.click();
        helper.waitTime(1);
        System.out.println("I'm in profile bar.");

        btn_sell.click();
        helper.waitTime(1);
        System.out.println("I'm in selling bar.");

        helper.waitTime(1);
        System.out.println("All bars are correct.");

//        try {
//            driver.findElement(By.id("pl.vinted:id/user_verification_phone"));
//        } catch (NoSuchElementException e) {
//            Assert.fail("Error: element not found!!");
//        }
//        Assert.assertTrue(driver.findElement(By.id("pl.vinted:id/user_verification_phone")).isDisplayed());
//        System.out.println("Element found. I'm continuing.");

    }

    @Test(dataProvider = "simpleData", description = "Checking UI Movement On All Pages")
    public void testAllPagesMovement(String name, String password) {

    //region DECLARATION
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        NewsScreen newsScreen = PageFactory.initElements(driver, NewsScreen.class);
        FindScreen findScreen = PageFactory.initElements(driver, FindScreen.class);
        SellScreen sellScreen = PageFactory.initElements(driver, SellScreen.class);
        MessagesScreen messagesScreen = PageFactory.initElements(driver, MessagesScreen.class);
        ProfileScreen profileScreen = PageFactory.initElements(driver, ProfileScreen.class);
    //endregion DECLARATION

    //region LOGIN
    loginScreen.loginToApp(name, password);
    //endregion LOGIN

    //region FINDBARTEST

        btn_find.click();
        findScreen.checkFindUI();
        helper.waitTime(1);

    //endregion FINDBARTEST

    //region MESSAGESTEST

        btn_messages.click();
        messagesScreen.checkMessagesUI();
        helper.waitTime(1);

    //endregion MESSAGESTEST

    //region PROIFILETEST

    btn_messages.click();
    profileScreen.checkProfileUI();
    helper.waitTime(1);

    //endregion PROFILETEST

    }

    @AfterMethod(description = "Finishing test and shutting down app.")
    public void tearDown() {
        helper.waitTime(3);
        System.out.println("End of UI movement test.");
    }
}

package com.selenium.java.tests.test.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.helper.TestListener;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.web.LoginPage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import java.lang.reflect.Method;

@Listeners({ TestListener.class })

public class LoginWebTest extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = true; //uncomment if u want a headless mode
        url = "https://www.vinted.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", "Tester123##"},
                {"lukasz_tester_2@wp.pl", "Tester123##"},
                {"lukasz_tester_333@wp.pl", "Tester123##"},
                {"lukasz_tester_444@wp.pl", "Tester123##"},
                {"lukasz_tester_555@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"lukasz_tester_1x1@wp.pl", ""},
                {"lukasz_tester_2x2wp.pl", ""},
                {"lukasz_tester_3x3@wp.pl", ""},
                {"lukasz_tester_4x4@wp.pl", ""},
                {"lukasz_tester_5x5@wp.pl", ""},
        };
    }

    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"", "Tester123##"},
                {"", "Tester13##"},
                {"", "Tester1##"},
                {"", "Tester23##"},
                {"", "Teter13##"},
        };
    }

    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"lukasz_tester_1@w@p.pl", "Tester123##"},
                {"lukasz_tester_1@wpp", "Tester123##"},
                {"lukasz_tester_1@", "Tester123##"},
                {"lukasz_tester_1", "Tester123##"},
                {"@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"lukasz_tester_1q1x@wp.pl", "tester123##"},
                {"lukasz_tester_2q2x@wp.pl", "tester"},
                {"lukasz_tester_3q3x@wp.pl", "tester123"},
                {"lukasz_tester_4q4x@wp.pl", "Tester"},
                {"lukasz_tester_5q5x@wp.pl", "lukasz_tester_5@wp.pl"},
        };
    }

    @Test(dataProvider = "getData", priority = 1, description = "Login test with correct username and correct password")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Correct username and correct password TEST")

    public void testLoginAccount(String name, String pass) {

        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(name, pass);
        helper.waitTime(2);

        try {
            driver.findElement(By.className("TU WSTAW CLASS NAME"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error on Page: element not found");
        }
        Assert.assertTrue(driver.findElement(By.className("TU WSTAW CLASS NAME")).isDisplayed());


        helper.getScreenShot("Login test with correct username and correct password");
        helper.getScreenshotForAllure("Test photo.png");
        driver.close();

    }



    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testów zakładania konta");
    }
}




